package prob6;

import java.util.*;
import java.util.stream.Collectors;

public class Main {


    public static void main(String[] args){
        Set<String> set1 = new HashSet<>();
        set1.add("A");
        set1.add("B");

        Set<String> set2 = new HashSet<>();
        set2.add("D");

        Set<String> set3 = new HashSet<>();
        set3.add("1");
        set3.add("3");
        set3.add("5");

        List<Set<String>> sets = Arrays.asList(set1,set2,set3);

        Main main = new Main();

        Set<String> unionSet = main.union(sets);

        System.out.println(unionSet);

    }

    public Set<String> union(List<Set<String>> sets){
//        Set<String> unionSet = sets.stream()
//                .flatMap(set->set.stream())
//                .collect(Collectors.toSet());

        Set<String> unionSet = sets.stream()
                .reduce(new HashSet<>(), (Set<String> s1,Set<String> s2)-> {s1.addAll(s2); return s1;});

//        unionSet.forEach(System.out::println);

        return unionSet;
    }
}
