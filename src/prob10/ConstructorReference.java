package prob10;

import prob7.partB.TriFunction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

class Human {
    String name;
    int age;
    String gender;

    public Human(String name) {
        this.name = name;
    }

    public Human(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Human(String name, int age, String gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Human [name=" + name + ", age=" + age + ", gender=" + gender + "]";
    }
}

public class ConstructorReference {
    public static void main(String args[]) {
        Human[] list = {new Human("Joe", 35, "Male"), new Human("Jane", 45, "Female"), new Human("John", 30, "Male")};

        // Query 1 : Print only Female candidates names
        System.out.println("\n----------------------------------------");
        System.out.println("Female Candidates");
        System.out.println("----------------------------------------");

        Arrays.stream(list)
                .filter(human -> human.getGender().equals("Female"))
                .map(Human::getName)
                .forEach(System.out::println);



        // Query 2 : Create an object by choosing suitable Interface to the specified constructors(Totally 3 constructors)using fourth type of Method Reference ClassName::new. Then print the object status
        System.out.println("\n----------------------------------------");
        System.out.println("Creating Object using ClassName::new Method Reference");
        System.out.println("----------------------------------------");

        Function<String, Human> objectByName = Human::new;
        BiFunction<String, Integer, Human> objectByNameAndAge = Human::new;
        TriFunction<String,Integer,String,Human> objectByNameAgeAndGender = Human::new;

        Human h1 = objectByName.apply("Ashok");
        Human h2 = objectByNameAndAge.apply("Ashok",26);
        Human h3 = objectByNameAgeAndGender.apply("Ashok",26,"male");

        System.out.println(h1);
        System.out.println(h2);
        System.out.println(h3);

         // Query 3 : Count the male candidates whose age is more than 30
        System.out.println("\n----------------------------------------");
        System.out.println("Male candidates with age more than 30");
        System.out.println("----------------------------------------");
        System.out.println(Arrays.stream(list)
                .filter(human -> human.getGender().equals("Male"))
                .filter(human -> human.getAge() > 30)
                .count());

    }

}
