package prob5;

import java.util.Arrays;
import java.util.stream.Stream;

public class Section {
    public static Stream<String> streamSection(Stream<String> stream, int m, int n) {
        // Implement the code
        return stream.skip(m).limit(n-m);
    }

    public static void main(String[] args) {
        // Make three calls for the sectionSection
        // use nextStream() method to supply the Stream input as a first argument in streamSection() method
        System.out.println("----------------------------------------");
        System.out.println("Call 1 (2,5)");
        System.out.println("----------------------------------------");
        Stream<String> call1 = streamSection(nextStream(),2,5);
        call1.forEach(System.out::println);

        System.out.println("----------------------------------------");
        System.out.println("Call 2 (0,4)");
        System.out.println("----------------------------------------");
        Stream<String> call2 = streamSection(nextStream(),0,4);
        call2.forEach(System.out::println);

        System.out.println("----------------------------------------");
        System.out.println("Call 3 (1,8)");
        System.out.println("----------------------------------------");
        Stream<String> call3 = streamSection(nextStream(),1,8);
        call3.forEach(System.out::println);

    }

    private static Stream<String> nextStream() {
        return Stream.of("aaa", "bbb", "ccc", "ddd", "eee", "fff", "ggg", "hhh", "iii");
    }
}
