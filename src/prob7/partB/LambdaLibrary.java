package prob7.partB;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

public class LambdaLibrary {

    public static final BiFunction<List<Employee>, Character, List<Employee>> EMPLOYEES_WITH_NAME_PAST_LETTER =
            (employees, nameChar) -> employees.stream()
                    .filter(employee -> employee.getLastName().charAt(0) > nameChar)
                    .collect(Collectors.toList());

    public static BiFunction<List<Employee>, Integer, List<Employee>> EMPLOYEES_WITH_SALARY_MORETHAN =
            (employees, salary) -> employees.stream()
                    .filter(employee -> employee.getSalary() > salary)
                    .collect(Collectors.toList());

    public static BiFunction<List<Employee>, Character, List<String>> EMP_NAMES_WITH_PAST_LETTER =
            (employees, nameChar) -> employees.stream()
                    .filter(employee -> employee.getLastName().charAt(0) > nameChar)
                    .map(employee -> employee.getFirstName() + " " + employee.getLastName())
                    .collect(Collectors.toList());

    public static BiFunction<List<Employee>, Integer, List<String>> EMP_NAMES_WITH_SALARY_MORETHAN =
            (employees, salary) -> employees.stream()
                    .filter(employee -> employee.getSalary() > salary)
                    .map(employee -> employee.getFirstName() + " " + employee.getLastName())
                    .collect(Collectors.toList());

    public static Function<List<String>, String> SORT_AND_JOIN_NAME = (employees) -> employees.stream()
            .sorted()
            .collect(Collectors.joining(", "));
}
