package prob3;

import prob7.partB.TriFunction;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class Best {
    final TriFunction<Character,Character,Integer, Predicate<String>> conditions =
            (char1,char2,len) -> str -> str.indexOf(char1) != -1 && str.indexOf(char2) == -1 && str.length() == len;

    public static void main(String[] args) {
        List<String> strings = Arrays.asList(
                "simply", "dummy", "printing", "typesetting", "standard", "content", "accident", "professor"
        );

        Good good = new Good();
        int count = good.countWords(strings, 'i', 'd', 6);

        System.out.println(count);
    }

    public int countWords(List<String> words, char c, char d, int len) {
        return
                (int) words.stream()
                        .filter(conditions.apply(c,d,len))
                        .count();

    }
}
