package prob3;

import java.util.Arrays;
import java.util.List;

public class Good {
    public static void main(String[] args){
        List<String> strings = Arrays.asList("simply","dummy","printing","typesetting","standard","content","accident","professor");

        Good good = new Good();
        int count = good.countWords(strings,'i','d',6);

        System.out.println(count);
    }

    public int countWords(List<String> words, char c, char d, int len){
        return
                (int) words.stream()
                .filter(str->str.length() == len)
                .filter(str-> str.indexOf(c) != -1)
                .filter(str-> str.indexOf(d) == -1)
                .count();

    }
}
