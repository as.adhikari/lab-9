package prob3;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class Better {
    final Function<Integer, Predicate<String>> isLenEqual = (len) -> str -> str.length() == len;

    final Function<Character, Predicate<String>> stringContainsChar = (chr) -> str -> str.indexOf(chr) != -1;

    final Function<Character, Predicate<String>> doesNotContainsChar = (chr) -> str -> str.indexOf(chr) == -1;

    public static void main(String[] args) {
        List<String> strings = Arrays.asList(
                "simply", "dummy", "printing", "typesetting", "standard", "content", "accident", "professor"
        );

        Good good = new Good();
        int count = good.countWords(strings, 'i', 'd', 6);

        System.out.println(count);
    }

    public int countWords(List<String> words, char c, char d, int len) {
        return
                (int) words.stream()
                        .filter(isLenEqual.apply(len))
                        .filter(stringContainsChar.apply(c))
                        .filter(doesNotContainsChar.apply(d))
                        .count();

    }
}
