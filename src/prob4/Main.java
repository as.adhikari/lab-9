package prob4;

import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args){
        System.out.println("------------------------------");
        System.out.println(" First Call (10)");
        System.out.println("------------------------------");
        IntStreamTest.printSquares(10);


        System.out.println("------------------------------");
        System.out.println(" Second Call (3)");
        System.out.println("------------------------------");
        IntStreamTest.printSquares(3);


        System.out.println("------------------------------");
        System.out.println(" Third Call (0)");
        System.out.println("------------------------------");
        IntStreamTest.printSquares(0);
    }
}

class IntStreamTest{
    public static void printSquares(int num){
         IntStream.iterate(1,n-> n+1).map(m->m*m).limit(num).forEach(System.out::println);
    }
}
