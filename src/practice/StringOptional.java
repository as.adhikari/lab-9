package practice;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringOptional {
    public static void main(String[] args){
        Stream strings = Stream.of("A", "good", "day", "to", "write", "some", "Java");

//        String concatValue = strings.reduce("",(s1, s2)->(s1+" "+s2).trim()).toString();

//        System.out.println(concatValue);

//        System.out.println(
//                strings
//                .collect(Collectors.joining(" "))
//        );

        System.out.println(
                strings
                .collect(Collectors.joining(", "))
        );

        Arrays.asList("Bill","Thomas","Mary").stream()
                .collect(Collectors.joining(","));

    }
}
