package practice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class Main {
    static enum SortMethod {BY_NAME, BY_SALARY}

    ;

    static final Function<Employee, String> byName = Employee::getName;
    static final Function<Employee, Integer> bySalary = Employee::getSalary;


    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Joe", 100000));
        employees.add(new Employee("Tim", 50000));
        employees.add(new Employee("Rick", 50000));
        employees.add(new Employee("Andy", 60000));
        employees.add(new Employee("Andy", 50000));
        employees.add(new Employee("Tim", 10000));

        List<Employee> sortedEmployees = sort(employees, SortMethod.BY_SALARY);

        for (Employee e : sortedEmployees) {
            System.out.println(e);
        }


    }

    public static List<Employee> sort(List<Employee> employees, SortMethod method) {

        if (method == SortMethod.BY_NAME) {
            Collections.sort(employees, Comparator.comparing(byName).thenComparing(bySalary));
        } else if (method == SortMethod.BY_SALARY) {
            Collections.sort(employees, Comparator.comparing(bySalary).thenComparing(byName));
        }

        return employees;
    }
}
