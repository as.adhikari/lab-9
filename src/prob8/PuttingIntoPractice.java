package prob8;

import java.util.Arrays;
import java.util.Comparator;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

public class PuttingIntoPractice {
    public static void main(String... args) {
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario", "Milan");
        Trader alan = new Trader("Alan", "Cambridge");
        Trader brian = new Trader("Brian", "Cambridge");

        List<Transaction> transactions = Arrays.asList(
                new Transaction(brian, 2011, 300),
                new Transaction(raoul, 2012, 1000),
                new Transaction(raoul, 2011, 400),
                new Transaction(mario, 2012, 710),
                new Transaction(mario, 2012, 700),
                new Transaction(alan, 2012, 950)
        );


        // Query 1: Find all transactions from year 2011 and sort them by value (small to high).
        System.out.println("\n----------------------------------------");
        System.out.println("1. All Transaction from year 2011");
        System.out.println("----------------------------------------");
//        List<Transaction> transactions1 = transactions.stream()
//                .filter(tran -> tran.getYear() == 2011)
//                .sorted((t1,t2) -> Integer.compare(t1.getValue(),t2.getValue()))
//                .collect(toList());

        List<Transaction> transactions1 = transactions.stream()
                .filter(tran -> tran.getYear() == 2011)
                .sorted(Comparator.comparing(Transaction::getValue))
                .collect(toList());

        for (Transaction transaction : transactions1) {
            System.out.println(transaction);
        }


        // Query 2: What are all the unique cities where the traders work?
        System.out.println("\n----------------------------------------");
        System.out.println("2. Unique Cities");
        System.out.println("----------------------------------------");
        List<String> uniqueCities = transactions.stream()
                .map(tran -> tran.getTrader().getCity())
                .distinct()
                .collect(toList());

        for (String city : uniqueCities) {
            System.out.println(city);
        }


        // Query 3: Find all traders from Cambridge and sort them by name.
        System.out.println("\n----------------------------------------");
        System.out.println("3.All Traders from Cambridge");
        System.out.println("----------------------------------------");
        List<Trader> tradersFromCambridge = transactions.stream()
                .map(Transaction::getTrader)
                .filter(trader -> trader.getCity().equals("Cambridge"))
                .collect(toList());

        for (Trader trader : tradersFromCambridge) {
            System.out.println(trader);
        }


        // Query 4: Return a string of all traders names sorted alphabetically.
        System.out.println("\n----------------------------------------");
        System.out.println("4.String of all the traders names");
        System.out.println("----------------------------------------");
        String tradersName = transactions.stream()
                .map(tran -> tran.getTrader().getName())
                .sorted()
                .collect(Collectors.joining(", "));

        System.out.println(tradersName);


        // Query 5: Are there any trader based in Milan?
        System.out.println("\n----------------------------------------");
        System.out.println("5. Are there any trader based in Milan?");
        System.out.println("----------------------------------------");

        boolean areAnyTradersFromMilan = transactions.stream()
                .anyMatch(tran -> tran.getTrader().getCity().equals("Milan"));

        System.out.println(areAnyTradersFromMilan);


        // Query 6: Update all transactions so that the traders from Milan are set to Cambridge.
        System.out.println("\n----------------------------------------");
        System.out.println("6.Updating transactions");
        System.out.println("----------------------------------------");

        List<Transaction> updatedTransactions = transactions.stream()
                .filter(tran -> tran.getTrader().getCity().equals("Milan"))
                .peek(tran -> tran.getTrader().setCity("Cambridge"))
                .collect(toList());

        for(Transaction tran:updatedTransactions){
            System.out.println(tran);
        }


        // Query 7: What's the highest value in all the transactions?
        System.out.println("\n----------------------------------------");
        System.out.println("Highest value in all the transactions");
        System.out.println("----------------------------------------");

        IntSummaryStatistics transactionSummary = transactions.stream()
                .collect(Collectors.summarizingInt(Transaction::getValue));

        System.out.println(transactionSummary.getMax());
    }
}
