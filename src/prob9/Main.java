package prob9;

import prob8.Transaction;

import java.util.List;
import java.util.Optional;
import java.util.function.BinaryOperator;

public class Main {
    public static void main(String[] main){
        List<Dish> menu = Dish.menu;

        //1. Is there any Vegetarian meal available (Return type boolean)
        System.out.println("--------------------------------------------");
        System.out.println("Is Vegetarian Meal Available?");
        System.out.println("--------------------------------------------");

        System.out.println(Dish.isAnyVegeterainMealAvailable());

        //2.Is there any healthy menu have calories less than 1000 (return type boolean)
        System.out.println("--------------------------------------------");
        System.out.println("Is Healthy Menu less than 1000 calories?");
        System.out.println("--------------------------------------------");
        System.out.println(Dish.isAnyHealthyMenuAvailable());

        //3. Is there unhealthy menu have calories greater than 1000
        System.out.println("--------------------------------------------");
        System.out.println("Is unhealthy Menu greater than 1000 calories?");
        System.out.println("--------------------------------------------");
        System.out.println(Dish.isAnyUnhealthyMeal());

        //4.Find and return the first item for the type of MEAT (return type Optional<Dish>)
        System.out.println("--------------------------------------------");
        System.out.println("First MEAT Item");
        System.out.println("--------------------------------------------");
        Optional<Dish> firstMeatItem = Dish.findFirstMeatIteam();
        System.out.println(firstMeatItem.isPresent());
        System.out.println(firstMeatItem.isPresent()?firstMeatItem.get():"No Item");

       // 5. calculateTotalCalories() in the menu using reduce (return int)
        System.out.println("--------------------------------------------");
        System.out.println("calculateTotalCalories in the Menu");
        System.out.println("--------------------------------------------");
        int totalCalories = Dish.calculateTotalCalories();
        System.out.println(totalCalories);

        // 6. calculateTotalCaloriesMethodReference() in the menu using MethodReference (return int)
        System.out.println("--------------------------------------------");
        System.out.println("calculateTotalCaloriesMethodReference in the Menu");
        System.out.println("--------------------------------------------");
        int totalCaloriesReference = Dish.calculateTotalCaloriesMethodReference();
        System.out.println(totalCaloriesReference);
    }

}
