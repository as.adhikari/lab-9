package prob2;

import java.util.IntSummaryStatistics;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args){

        Stream<Integer> integerStream = Stream.of(200,63,4,10,38,47,33,28,9,25,49);

//        IntSummaryStatistics intSummaryStatistics = integerStream.collect(Collectors.summarizingInt(Integer::intValue));
        IntSummaryStatistics intSummaryStatistics = integerStream.collect(Collectors.summarizingInt(Integer::new));

        System.out.println("Maximum Value: "+intSummaryStatistics.getMax());
        System.out.println("Minimum Value: "+intSummaryStatistics.getMin());
        System.out.println("Average Value: "+intSummaryStatistics.getAverage());
    }
}
